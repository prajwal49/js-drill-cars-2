import { inventory } from "../data.js";
import { getBMWAndAudiCars } from "../problem6.js";

try {
  console.log(JSON.stringify(getBMWAndAudiCars(inventory,["BMW", "Audi"])));
} catch (error) {
  console.log(error.message);
}
