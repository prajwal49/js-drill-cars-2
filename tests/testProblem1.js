import { inventory } from "../data.js";
import { getCarDetailsById } from "../problem1.js";

try {
    const carDetails = getCarDetailsById(inventory, 33);
    console.log(carDetails);
} catch (error) {
    console.error(error.message);
}
