import { inventory } from "../data.js";
import { getYearOfAllCar } from "../problem4.js"
import { getCarWhichIsOlderThan2000 } from "../problem5.js"

try {
    console.log(getCarWhichIsOlderThan2000(getYearOfAllCar(inventory),2000));
} catch (error) {
    console.log(error.message);
}