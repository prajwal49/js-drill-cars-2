export function getMakeAndModelYearOfLastCar(data) {
  if (data.length === 0) return "No car in the Inventory";
  if (!Array.isArray(data)) {
    throw new Error("Input must be an array");
  }
  let lastIdx = data.length - 1;

  return `Last car is a ${data[lastIdx].car_make} ${data[lastIdx].car_model}`;
}
