export function getYearOfAllCar(data) {
  if (!Array.isArray(data)) {
    throw new Error("input data must be array");
  }

  return data.map((car) => {
    if (!car || typeof car.car_year === "undefined") {
      throw new Error("Each car obj should have car year");
    }
    return car.car_year;
  });
}
