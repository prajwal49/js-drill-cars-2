export function getCarWhichIsOlderThan2000(car_year, year) {
  if (!Array.isArray(car_year)) {
    throw new Error("Input must be a array");
  }

  const result = car_year.filter((car_year) => {
    if (typeof car_year !== "number") {
      throw new Error("Car year must be a number");
    }
    return car_year < year;
  });
  return result;
}
