export function sortCarModelAlphabetically(data) {
  if (!Array.isArray(data)) {
    throw new Error("Input data must be array");
  }

  return data
    .map((item) => {
      if (typeof item.car_model !== "string") {
        throw new Error(
          "Each car object must have a 'car_model' property of type string"
        );
      }
      return item;
    })
    .sort((a, b) => a.car_model.localeCompare(b.car_model))
    .map((item) => item.car_model);
}
