export function getBMWAndAudiCars(data, carMakers) {
    if (!Array.isArray(data)) {
        throw new Error("Input must be an array");
    }

    if (!Array.isArray(carMakers)) {
        throw new Error("Car makers must be an array");
    }

    const result = data.filter((car) => {
        if (typeof car.car_make !== 'string') {
            throw new Error("Each car must have a 'car_make' property of type string");
        }
        return carMakers.includes(car.car_make);
    });

    return result;
}
