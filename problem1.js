export function getCarDetailsById(data, id) {
  if (!Array.isArray(data)) {
    throw new Error("Input data must be array");
  }
  const car = data.find((item) => item.id === id);
  if (car) {
    return `Car is a ${car.car_year} ${car.car_make} ${car.car_model}`;
  } else {
    throw new Error(`Car with ID ${id} not found`);
  }
}
